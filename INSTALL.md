## Installation

There are many options for installing kwruntime. You can install in any nodejs supported platform: Linux, Windows, Mac, Android, etc.

In Android you can install using Termux  (recommended Termux from F-Droid).

Available options for install:

1. **Linux**. Now use ```nvm``` for install.

	```bash
	curl https://gitlab.com/kwruntime/installer/-/raw/main/scripts/linux.sh | bash
	``` 

3. **Mac OS** (supported: ```x64, arm64```)

	```bash
	curl https://gitlab.com/kwruntime/installer/-/raw/main/scripts/mac.sh | bash
	``` 

4. **Windows** 

	Windows Installers:
	
	- [Windows 10](https://scripts.kode.best/s/media/-/programs/KwRuntime/installer/KwRuntime-windows10.zip).

	_Obsoleto. Sin soporte_
	- [Windows 7](https://scripts.kode.best/s/media/-/programs/KwRuntime/installer/KwRuntime-windows7.zip).

	The installer automatically download and install latest version.

5. **Android** (with Termux)

	You can use this way if "using node.js" not working
	```bash 
	curl https://gitlab.com/kwruntime/installer/-/raw/main/scripts/android.sh | bash
	```

## Installer project

If you are interested on see the source code of installer see here: [kwruntime/installer](https://gitlab.com/kwruntime/installer)